<?php

declare(strict_types=1);
/**
 * Reference implementation of a credit commons node
 */
require_once './vendor/autoload.php';

$settings = parse_ini_file('./murmagg.ini');
global $db_conn;
$db_conn = new mysqli(
  hostname: $settings['db_host'],
  username: $settings['db_user'],
  password: $settings['db_pass'],
  database: $settings['db_name']
);

require './slimapp.php';

$app->run();
