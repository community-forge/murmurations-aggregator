<?php

namespace MurmAgg;
// Couldn't get the JsonSchema validation to work...
use Opis\JsonSchema\{
    Validator,
    ValidationResult
};

abstract class Base {

  function __construct(
    public string $schema_version,
    public \stdClass $geolocation,
    public string $profile_url,
    public string $primary_url,
    public string $locality,
    public string $region,
    public string $country,
  ) {
  }

  /**
   * @param \stdClass $profile
   * @return static
   * @todo clarify whether the lat and lon should be under 'geolocation'
   * @note Base fields should have already been validated by the index.
   */
  public static function createFromProfile(\stdClass $profile) : static {
//    if (!empty($profile->lat) and !empty($profile->lon)) {
//      static::checkCoords($profile->lat, $profile->lon);
//    }
    if (!filter_var($profile->profile_url, FILTER_VALIDATE_URL) !== FALSE) {
      throw new \Exception("profile_url is not a valid url: $profile->profile_url");
    }
    return new static(
      schema_version: static::schemaVersion(),
      geolocation: (object)[
        'lat' => (float)$profile->geolocation->lat,
        'lon' => (float)$profile->geolocation->lon
      ],
      profile_url: $profile->profile_url,
      primary_url: $profile->primary_url,
      locality: $profile->locality??'',
      region: $profile->region??'',
      country: $profile->country??'',
    );
  }


  public function write() : int {
    global $db_conn;
    $query = "REPLACE INTO nodes (schema_version, lat, lon, locality, region, country, profile_url, primary_url) VALUES ";
    $query .= "('$this->schema_version', '{$this->geolocation->lat}', '{$this->geolocation->lon}', '$this->locality', '$this->region', '$this->country', '$this->profile_url', '$this->primary_url')";
    $result = $db_conn->query($query);
    if (!$result) {
      throw new \Exception(mysqli_error($db_conn) ."\n$query");
    }
    return $db_conn->insert_id;
  }


  /**
   *
   * @param string $profile_url
   * @return int
   *   The database id of the deleted item
   */
  public static function delete(string $profile_url) : int {
    global $db_conn;
    $num = 0;
    if ($result = $db_conn->query("SELECT id FROM nodes WHERE profile_url = '$profile_url'")->fetch_object()) {
      if ($num = $result->id) {
        $db_conn->query("DELETE FROM nodes WHERE id = '$num'");
      }
      else print_r(get_class_methods($result));
    }
    return $num;
  }

  /**
   * Validate against the json schema.
   *
   * @see https://opis.io/json-schema/2.x/quick-start.html
   * @note another way to validate: https://app.swaggerhub.com/apis-docs/MurmurationsNetwork/IndexAPI/2.0.0#/Node%20Endpoints/post_validate
   * @note The base fields are assumed to be already validated by the index.
   * @todo cache the schema somehow.
   */
  static function validate($json) {
return TRUE;
    $validator = new Validator();
    $validator->resolver()->registerFile(
      static::schemaName(), // This is a unique identifier (the url is fine)
      static::SCHEMA_URL // A string filename that goes in a list.
    );

    // Unresolved reference: https://test-cdn.murmurations.network/fields/linked_schemas.json#
    // The $id at https://github.com/MurmurationsNetwork/MurmurationsLibrary/blob/test/schemas/offers_wants_prototype-v0.0.2.json does not have the hash (#) at the end of it. Why is your schema validator adding it?

    /** @var ValidationResult $result */
    $result = $validator->validate(
      json_decode($json),
      // a string representing an uri-reference or a json-encoded schema
      // or an instance of Opis\JsonSchema\Schema or Opis\JsonSchema\Uri
      file_get_contents(static::SCHEMA_URL)
    );
    if (!$result->isValid()) {
      throw new \Exception($result->error());
      // Print errors
      //print_r((new ErrorFormatter())->format($result->error()));
    }
  }


  public static function buildQueryConditions(array $params) : array {
    $where = [];
    $exact_matches = ['locality', 'region', 'country'];
    foreach ($exact_matches as $fname) {
      if (isset($params[$fname])) {
        $where[] = "$fname = '".$params[$fname]."'";
      }
    }
    if (isset($params['lat']) and isset($params['lon']) and isset($params['range'])) {
      list ($lat, $lon) = static::checkCoords($params['lat'], $params['lon']);
      $radius = preg_match('/^([0-9.]+)([a-zA-Z]*)$/', $params['range'], $matches);
      $radius = $matches[1];
      if ($matches[2] and $matches[2] <> 'km') {
        throw new \Exception('unknown unit of distance: '.$matches[2]);
      }
      if ($radius <= 0) {
        throw new \Exception('Invalid radius: '.$params['range']);
      }
      // One degree of latitude is about 111.11 km
      $lat_var = $radius / 111;
      $min_lat = $lat- $lat_var;
      $max_lat = $lat + $lat_var;
      // Making a box is much easier than filtering by radius!
      //  the distance between two lines of longitude is about 111*cos(latitude) km
      $lon_var = $radius / abs(cos($params['lat'])*111);
      $min_lon = $lon - $lon_var;
      $max_lon = $lon + $lon_var;
      //$query .=  " AND st_within(location, envelope(linestring($rlon1 $rlat1, $rlon2 $rlat2)))";
      $where[] = "(lat > $min_lat AND lat < $max_lat AND lon > $min_lon and lon < $max_lon)";
    }
    return $where;
  }

  /**
   * @todo derive the schema name
   */
  private static function schemaName() {
    $parts = explode('/', static::SCHEMA_URL);
    return str_replace('.json', '', array_pop($parts));
  }

  private static function schemaVersion() {
    $parts = parse_url(static::SCHEMA_URL);
    preg_match('/-v([0-9]\.[0-9]\.[0-9]+)\.json/', static::SCHEMA_URL, $matches);
    return $matches[1];
  }
  public static function dbTableName() {
    preg_match('/^(.*)-v[0-9]/',  static::schemaName(), $matches);
    return $matches[1];
  }

  /**
   * Validate latitude and longitude and convert to floats
   *
   * @param string $lat
   * @param string $lon
   * @return array
   * @throws \Exception
   */
  protected static function checkCoords(string $lat, string $lon) : array {
    $lat = $lat;
    if (!is_numeric($lat) or $lat < -90 or $lat > 90) {
      throw new \Exception("Latitude out of range: $profile->lat");
    }
    $lon = (float)$lon;
    if (!is_numeric($lon) or $lon < -180 or $lon > 180) {
      throw new \Exception("Longitude out of range: $profile->lon");
    }
    return [(float)$lat, (float)$lon];
  }

  abstract static function filter(array $params, int $page = 0, int $page_size = 30) : array;


  static function buildQuery(array $params, int $page = 0, int $page_size = 30) : string {
    $query = "SELECT *, 0 AS distance ";
    $query .= " FROM nodes n LEFT JOIN ". static::DB_TABLE_NAME ." schema_table ON n.id = schema_table.id ";
    if ($where = static::buildQueryConditions($params)) {
      $query .= ' WHERE '.implode(' AND ', $where);
    }
    if (isset($params['range']) and isset($params['lat']) and isset($params['lon'])) {
      $src_lat = $params['lat'];
      $src_lon = $params['lon'];
      $dist_field = "111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS($src_lat))
         * COS(RADIANS(lat))
         * COS(RADIANS($src_lon - lon))
         + SIN(RADIANS($src_lat))
         * SIN(RADIANS(lat))))) AS distance";
      // Currently there is no ordering, so we always order by distance.
      $query = str_replace('0 AS distance', "$dist_field", $query) . ' ORDER BY distance ASC ';
    }
    $offset = $page * $page_size;
    $query .= " LIMIT $offset, $page_size";
    return $query;
  }

  protected static function countQuery(string $query) : int {
    global $db_conn;
    $count_query = str_replace('SELECT *', 'SELECT COUNT(*) AS num', $query);
    $count_query = preg_replace('/ LIMIT [0-9]+, [0-9]+/', '', $count_query);
    if ($result = $db_conn->query($count_query)) {
      return $result->fetch_object()->num;
    }
    return 0;
  }

}

ini_set('display_errors', 1);
