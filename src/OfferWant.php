<?php

namespace MurmAgg;

class OfferWant extends Base {

  const SCHEMA_URL = 'https://raw.githubusercontent.com/MurmurationsNetwork/MurmurationsLibrary/test/schemas/offers_wants_prototype-v0.0.2.json';

  const DB_TABLE_NAME = 'offers_wants_prototype';
  const DB_TABLE = "
    `id` int NOT NULL,
    `title` varchar(127) NOT NULL,
    `description` mediumtext NOT NULL,
    `tags` varchar(255),
    `geographic_scope` enum('local','regional', 'national', 'international') NOT NULL,
    `exchange_type` enum('offer','want') NOT NULL,
    `item_type` enum('good','service') NOT NULL,
    `transaction_type` varchar(47) NOT NULL,
    `details_url` varchar(123)
  ";

  public string $title;
  public string $description;
  public string $tags;
  public string $exchange_type;
  public string $item_type;
  public string $transaction_type;
  public string $geographic_scope;
  public string $details_url = '';

  public static function createFromProfile(\stdClass $profile) : static {
    $item = parent::createFromProfile($profile);
    $item->title = (string)$profile->title;
    $item->description = (string)$profile->description;
    $item->tags = implode(',', $profile->tags);
    $item->item_type = $profile->item_type;
    $item->exchange_type = $profile->exchange_type;
    $item->transaction_type = implode(',',$profile->transaction_type);
    $item->geographic_scope = $profile->geographic_scope;
    if (isset($profile->details_url)) {
      $item->details_url = (string)$profile->details_url;
    }
    return $item;
  }

  public function write() :int {
    global $db_conn;
    $id = parent::write();
    $query = "REPLACE INTO offers_wants_prototype (id, title, description, tags, exchange_type, item_type, transaction_type, details_url) VALUES ";
    $query .= "($id, '".$db_conn->real_escape_string($this->title)."', '".$db_conn->real_escape_string($this->description)."', '".$db_conn->real_escape_string($this->tags)."', '$this->exchange_type', '$this->item_type', '$this->transaction_type', '$this->details_url')";
    $result = $db_conn->query($query);
    if (!$result) {
      mail_error('Database query failed', mysqli_error($db_conn) ."\n$query");
    }
    return $id;
  }

  public static function delete(string $profile_url) : int {
    global $db_conn;
    if ($id = parent::delete($profile_url)) {
      $db_conn->query("DELETE FROM offers_wants_prototype WHERE id = '$num->id'");
    }
    return $id;
  }

  public static function filter(array $params, int $page = 0, int $page_size = 30) : array {
    global $db_conn;
    $results = [];
    $num_results = 0;
    $query = parent::buildQuery($params, $page, $page_size);
    if ($num_results = static::countQuery($query)) {
      $result = $db_conn->query($query);
      while ($row = $result->fetch_object()) {
        // format the results;
        $results[] = static::formatResult($row);
      }
    }
    return [$results, (int)$num_results];
  }


  public static function buildQueryConditions(array $params) : array {
    $where = parent::buildQueryConditions($params);
    $params = array_filter($params); // No empty conditions will be used.
    $include_fields = ['title', 'description', 'tags'];
    $exact_fields = ['exchange_type', 'item_type'];
    foreach ($exact_fields as $fname) {
      if (isset($params[$fname])) {
        $where[] = "$fname = '".$params[$fname]."'";
      }
    }
    foreach ($include_fields as $fname) {
      if (isset($params[$fname])) {
        $where[] = "$fname LIKE '%".$params[$fname]."%'";
      }
    }
    if (isset($params['text'])) {
      $where[] = "(title LIKE '%".$params['text']."%' OR description LIKE '%".$params['text']."%' OR tags LIKE '%".$params['text']."%')";
    }
    if (isset($params['transaction_type'])) {
      $types = explode(",", $params['transaction_type']);
      $types = "'". implode("','", $types). "'";
      $where[] = "transaction_type IN ($types)";
    }
    return $where;
  }

  /**
   * Format the db query results into the format required by the API.
   * @param \stdClass $row
   *   stdClass Object
   *     [id] => 1
   *     [schema_version] => 0.0.2
   *     [lat] => 46.205
   *     [lon] => 6.10907
   *     [locality] =>
   *     [region] =>
   *     [country] => FR
   *     [primary_url] => https://hamlets-fr.communityforge.net/ad/31
   *     [title] => Beautiful stress-tested for hire.
   *     [description] => <p>Antehabeo consequat fere metuo odio pertineo ratis. </p>
   *     [tags] => Informatique & Eléctro,Sports & Evasion,offer,good
   *     [exchange_type] => offer
   *     [item_type] => good
   *     [transaction_type] => buy-sell
   *     [details_url] =>
   *     [geographic_scope] => local
   *   )
   */
  protected static function formatResult(\stdClass $row) : \stdClass {
    if ($row->lat and $row->lon) {
      $row->geolocation = ['lat' => $row->lat, 'lon' => $row->lon];
      unset($row->lat, $row->lon);
    }
    if ($row->locality or $row->region or $row->country) {
      $row->location = ['locality' => $row->locality, 'region' => $row->region, 'country' => $row->country];
      unset($row->locality, $row->region, $row->country);
    }
    $item = (array)$row;
    unset($item['id'], $item['schema_version'], $item['profile_url']);

    return (object)$item;
  }

}

