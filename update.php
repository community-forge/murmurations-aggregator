<?php
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;

ini_set('display_errors', 1);
const SETTINGS_FILE = './murmagg.ini';
require_once './vendor/autoload.php';

/**
 * update.php
 * get the updated nodes from the index, poll, and store.
 */
$settings = parse_ini_file(SETTINGS_FILE);
global $db_conn;
$db_conn = new mysqli(
  hostname: $settings['db_host'],
  username: $settings['db_user'],
  password: $settings['db_pass'],
  database: $settings['db_name']
);
$start_time = time();

check_db_tables($settings['schemas']);
/*
 * Fetch the recently changed nodes from the index
 */
$last_cron = $settings['last_cron'] ?: time(); // Nowhere better to store this at the moment
$url_parts = parse_url($settings['index_url']);
$client = new Client(['base_uri' => $url_parts['scheme'].'://'.$url_parts['host'], 'timeout' => 3]);
// Makes one request per supported schema.
foreach ($settings['schemas'] as $schema_name => $class_name) {
  try {
    echo $settings['index_url']."?last_updated=$last_cron&schema=$schema_name";
    $response = $client->get($url_parts['path']."?last_updated=$last_cron&schema=$schema_name");
    if ($response->getStatusCode() <> 200) {
      throw new \Exception('Unexpected response code from '. $url_parts['path']. ': '.$response->getStatusCode());
    }
  }
  catch (ConnectException $e) {
    mail_error('Connection to '.$url_parts['path'], $e->getMessage());
  }
  catch (ClientException|ServerException $e) {// All 400 errors should be in our own format
    mail_error($e->getStatusCode() . ' Exception on '.$url_parts['path'], $e->getMessage());
  }
  catch (\Exception $e) {echo $e->getMessage();
    mail_error('Problem with response from '.$url_parts['path'], $e->getMessage());
  }
  $raw_result = $response->getBody()->getContents();
  $items = json_decode($raw_result);
  //rewrite the setting file with current time as last_cron
  $settings_file = file_get_contents(SETTINGS_FILE);
  // this replacement is ugly
  $settings_file = preg_replace('/last_cron = ([0-9]+)/', 'last_cron = '.time(), $settings_file);
  file_put_contents(SETTINGS_FILE, $settings_file);
  if ($items->data) {
    if ($items->meta->number_of_results) {
      foreach ($items->data as $item) {
        $profile_urls[] = '("'.$item->profile_url.'", "'.$item->status.'", "'. mysqli_escape_string($db_conn, $class_name).'")';
      }
      $query = 'REPLACE INTO queue (profile_url, status, class) VALUES '. implode(', ', $profile_urls);
      $result = $db_conn->query($query);
      if (!$result) {
        mail_error('Database query failed ', mysqli_error($db_conn) ."\n$query");
      }
    }
  }
}

// Now process the queue for the remainder of 30 seconds.
$query = "SELECT profile_url, status, class FROM queue";
$result = $db_conn->query($query);
if ($err = mysqli_error($db_conn)) {
  mail_error('Database query failed', $err ."\n$query");
}
if ($result) {
  // Retrieve the full profiles and save them, removing them from the queue.
  while ($item = $result->fetch_object() and time() < $start_time + 30) {
    $class = $item->class;
    if ($item->status == 'deleted') {
      // If the status isn't 'posted' delete the item
      murm_cancel($class, $item->profile_url);
      continue;
    }
    $url_parts = parse_url($item->profile_url);
    $client = new Client(['base_uri' => $url_parts['scheme'].'://'.$url_parts['host'], 'timeout' => 3]);
    try {
      $response = $client->get($url_parts['path']);
    }
    catch (ConnectException $e) {
      continue;
    }
    catch(RequestException $e) {
      continue;
    }
    catch(ServerException $e) {
      continue;
    }
    catch(ClientException $e) {
      if ($e->getCode() == 404) {
        murm_cancel($class, $item->profile_url);
        continue;
      }
      else print $e->getMessage();
    }

    if ($response->getStatusCode() == 200 and $json = $response->getBody()->getContents()) {
      try {
        $class::validate($json);
      } catch(\Exception $e) {
        mail_error('Bad profile data', $e->getMessage());
      }
      $schema_class = $class::createFromProfile(json_decode($json));

      try {
        $schema_class->write();
      } catch (\Exception $e) {
        mail_error('Database write failed', $e->getMessage());
      }
    }
    $db_conn->query("DELETE FROM queue WHERE profile_url = '$item->profile_url'");
  }
}

exit;

function mail_error($subject, $description) {
  if (function_exists('sendmail')) {
    $settings = parse_ini_file('./murmagg.ini');
    mail($settings['admin_mail'], $subject, $description);
  }
  echo $subject;
  echo "\n<br />$description";
  exit;
}

function check_db_tables($schemas) {
  foreach ($schemas as $class) {
    $table_name = $class::dbTableName();
    if (!db_table_exists($table_name)) {
      check_base_tables();
      create_table($table_name, $class::DB_TABLE);
    }
  }
}
function check_base_tables() {
  global $db_conn;
  if (!db_table_exists('nodes')) {
    create_table('nodes', "
      `id` int NOT NULL,
      `schema_version` varchar(32) NOT NULL,
      `last_validated` timestamp NOT NULL,
      `lat` float DEFAULT NULL,
      `lon` float DEFAULT NULL,
      `locality` varchar(32) NOT NULL,
      `region` varchar(32) NOT NULL,
      `country` varchar(32) NOT NULL,
      `primary_url` varchar(64) NOT NULL,
      `profile_url` varchar(64) NOT NULL
    ");
    $db_conn->query('ALTER TABLE nodes ADD PRIMARY KEY (id)');
    $db_conn->query('ALTER TABLE nodes ADD UNIQUE KEY (profile_url)');
    $db_conn->query('ALTER TABLE nodes MODIFY id int NOT NULL AUTO_INCREMENT');
    create_table('queue', "
      `profile_url` varchar(128) NOT NULL,
      `status` varchar(15) NOT NULL,
      `class` varchar(31) NOT NULL
    ");
  }
}

function create_table($name, $fields) {
  global $db_conn;
  $db_conn->query(
    "CREATE TABLE `$name` ($fields) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;"
  );
}


function db_table_exists($table_name) : bool {
  global $db_conn;
  $db_conn->query("CALL sys.table_exists('$table_name', 'node', @table_type);");
  return (bool)$db_conn->query("SELECT @table_type;")->fetch_object()->{'@table_type'};
}

function murm_cancel(string $class, string $profile_url) {
  global $db_conn;
  $class::delete($profile_url);
  $db_conn->query("DELETE FROM queue WHERE profile_url = '$profile_url'");
}

