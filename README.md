# Murmurations Aggregator

A REST service which reads from a murmurations index, crawls murmurations pages and stores them, and which can then be queried. This first iteration only supports the offer_want schema, but extending it to other schemas is very easy.

## requirements
- PHP 8
- Composer
- Web server
- Mysql

## Installation
- Create a virtualhost on your web server.
- Download this package to the document root of the virtual host. e.g. with git clone git@gitlab.com:community-forge/murmurations-aggregator.git
- run 'composer update'
- Put the database settings into murmagg.ini.example and rename to murmagg.ini (The database will be set up the first time update is run.)
- Make murmagg.ini writeable by the web server (usually www-data) because this is where it stores the last_cron.
- In your browser visit update.php or /update. It may take some time. Check that the database has been created.
- Set a cron job to call update.php every few minutes.
