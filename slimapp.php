<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

// Using Slim 3 until I can work out how to get testing
// Seems like https://github.com/thephpleague/openapi-psr7-validator/tree/master/src/PSR15/SlimAdaptor is not compatible
$app = new \Slim\App();
$c = $app->getContainer();


/**
 * A list of schemas and versions
 */
$app->options('/', function (Request $request, Response $response, $args) {
  global $settings;
  $endpoints = array_keys($settings['schemas']);
  $response->getBody()->write(json_encode($endpoints));
  return $response;
}
)->setName('options');

$app->get('/', function (Request $request, Response $response) {
  header('Location: index.html');
  exit;
});

$app->get('/update', function (Request $request, Response $response, $args) {
  require './update.php';
  return $response->withStatus(200);
})->setName('update');

/**
 * Filter the nodes
 */
$app->get('/{schema}', function (Request $request, Response $response, $args) {
  global $settings;
  if (!isset($settings['schemas'][$args['schema']])) {
    return $response->withStatus(400 );
  }
  // assumes page_size is NOT already set to zero, which would crash.
  $params = $request->getQueryParams() + ['page' => 0, 'page_size' => 30];
  $class = $settings['schemas'][$args['schema']];
  $page = (int)$params['page'];
  $page_size = (int)$params['page_size'];
  unset($params['page'], $params['page_size']);
  // can we check the params agains the schema?
  [$results, $total] = $class::filter($params, $page, $page_size);

  $contents = [
    'data' => $results,
    'meta' => [
      'number_of_results' => (int)$total,
      'total_pages' => ceil($total/$page_size)
    ]
  ];
  $response->getBody()->write(json_encode($contents));

  return $response->withHeader('Content-Type', 'application/json');
})->setName('filter');


return $app;
