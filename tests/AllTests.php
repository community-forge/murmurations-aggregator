<?php
namespace MurmAgg\Tests;

use League\OpenAPIValidation\PSR15\ValidationMiddlewareBuilder;
use League\OpenAPIValidation\PSR15\SlimAdapter;
use PHPUnit\Framework\TestCase;
use \Nyholm\Psr7\Factory\Psr17Factory;

$settings = parse_ini_file(__DIR__.'/../murmagg.ini');
global $db_conn;
$db_conn = new \mysqli(
  hostname: $settings['db_host'],
  username: $settings['db_user'],
  password: $settings['db_pass'],
  database: $settings['db_name']
);

class AllTests extends TestCase {

  const SLIM_PATH = __DIR__.'/../slimapp.php';
  const API_FILE_PATH = __DIR__.'/../murm_aggregator.openapi.yml';

  function testFilter() {
    $body = $this->sendRequest('offers_wants_prototype-v0.0.2');
    if ($body->meta->number_of_results) {
      $item = reset($body->data);
    }
    else {
      die('Stopped testing: No results from filter');
    }
  }




  protected function sendRequest($path, array $params = []) : \stdClass|NULL|array {
    $request = $this->getRequest($path, 'GET');
    if (isset($params)) {
      $request = $request->withQueryParams($params);
    }
    $response = $this->getApp()->process($request, (new \Slim\Container)->get('response'));
    $response->getBody()->rewind();
    return json_decode($response->getBody()->getContents());
  }



  protected function getRequest($path, $method = 'GET') {
    $psr17Factory = new Psr17Factory();
    return $psr17Factory->createServerRequest($method, '/'.$path);
  }


  /**
   *
   * @staticvar string $app
   *   address of api file relative to the application root
   * @param type $api_file
   * @return \Slim\App
   */
  protected function getApp(): \Slim\App {
    static $app;
    if (!$app) {
      $app = require_once static::SLIM_PATH;
      if (static::API_FILE_PATH) {
        $spec = file_get_contents(static::API_FILE_PATH);
        $psr15Middleware = (new ValidationMiddlewareBuilder)
          ->fromYaml($spec)
          ->getValidationMiddleware();
        $middleware = new SlimAdapter($psr15Middleware);
        $app->add($middleware);
      }
    }
    return $app;
  }

}



